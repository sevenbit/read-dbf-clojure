(ns dbf2fdb.core
  (:gen-class)
  (:use [clojure.pprint])
  (:use [clojure.string :only (trim)]))

(declare read-db! get-dbf)

(defn -main
  [& args]
  (case (count args)
    1 (pprint (read-db! (get-dbf (first args))))
    (println "invalid arguments count!")))

(defn get-dbf 
  "create dbf object by filename"
  [file-name] 
  (org.xBaseJ.DBF. file-name))

(defn columns
 "get list db columns"
 [dbf]
  (let [field-count (.getFieldCount dbf)]
    (for [i (range 0 field-count) 
          :let [col (.getField dbf (inc i))]]
      col)))

(defn read-col
  "read value from current row and dbf column"
  [col]
  (let [col-type (.getType col)]
    (case col-type
      \C (String. (.getBytes col) "cp866")  ;char field
      (.get col))))           

(defn read-row
  "read row from dbf and convert to map {:column-name value}"
  [dbf cols]
  (.read dbf)
  (zipmap (map #(.getName %) cols) (map read-col cols)))  

(defn read-db! 
  "read all rows from db-file"
  [dbf]
  (let [record-count (.getRecordCount dbf)
        cols (columns dbf)]
    (repeat record-count (read-row dbf cols))))

;;for testing purposes 
;;(def dbf (get-dbf "test.DBF")#)
;;(def cols (columns dbf))
